# ansible-autoreboot  
Schedule a reboot command at the choosen time.  

# Supported variables
The module supports the following variables:  
- autoreboot_command: The command to run to reboot the system. /sbin/cron by default  
- autoreboot_schedule_minute: Minute part of the cron schedule. Defaults to 23
- autoreboot_schedule_hour: Hour part of the cron schedule. Defaults to 2
- autoreboot_schedule_day: Day part of the cron schedule. Defaults to *
- autoreboot_schedule_weekday: Weekday part of the cron schedule. Defaults to *
- autoreboot_schedule_month: Month part of the cron schedule. Default to *


